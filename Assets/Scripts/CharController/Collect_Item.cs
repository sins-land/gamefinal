﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collect_Item : MonoBehaviour
{
    public enum ItemType 
     {
        ItemCollect,
        MissionItemCollect

     }

    public ItemType type;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        switch(type)
        {
            case ItemType.ItemCollect:
                break;

            case ItemType.MissionItemCollect:
                break;
        }
        Destroy(gameObject);
    }

}
