﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationChar : MonoBehaviour
{
    private Animator anim;

    private void Start()
    {
        anim = GetComponent < Animator > ();
    }


    private void FixedUpdate()
    {
        

        if(Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.RightArrow))
        {
            anim.SetBool("isWalk", true);
        }
        else 
        {
            anim.SetBool("isWalk", false);
        }
        if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.D))
        {
            anim.SetBool("isWalk", true);
        }
        else
        {
            anim.SetBool("isWalk", false);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            anim.SetTrigger("Jump");
        }
    }

   
}
