﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public float Score { get; private set;}
    public void QuitButton()
    {
        Application.Quit();
    }

    public void NextScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    
    public void PreviousScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }
    
    public void RestartScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName); 
    }

    public void AddScore()
    {
        Score++;
        print($"Score: {Score}");
    }


}
